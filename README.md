## Install

- copy ``.secrets.dist`` to ``.secrets`` and update it.
- install virtualbox and vagrant
- ``vagrant plugin install vagrant-reload``
- ``./start.sh``

## Gitlab runner logs

- ``vagrant ssh``
- ``journalctl -u gitlab-runner.service -f``